package nl.bioinf.appdesign.b_refactor;

/**
 * Creation date: Sep 26, 2017
 *
 * @author Michiel Noback (&copy; 2017)
 * @version 0.01
 */
public class View {
    public Rectangle rectangle;
    public void redraw() {
        System.out.println(rectangle);
    }
}
package nl.bioinf.appdesign.a_test;

/**
 * Creation date: Sep 26, 2017
 *
 * @author Michiel Noback (&copy; 2017)
 * @version 0.01
 */

/**
 * This class encapsulates a geographic coordinate, with latitude and longitude positions in numeric degrees.
 * It provides several methods to calculate the distance between two coordinates: Euclidean, Haversine and Cosine law.
 * @author Michiel Noback (www.cellingo.net || michiel@cellingo.net)
 * @version 1.0
 */
public class Coordinate {
    private double latitude;
    private double longitude;

    /**
     * Constructor constructs with latitude and longitude positions.
     * @param latitude
     * @param longitude
     */
    public Coordinate(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * Returns the latitude position.
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Returns the longitude position.
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Calculates the Euclidean distance between this and another Coordinate.
     * Assumes one degree is about 111 kilometers and therefore only applies around the equator!
     * @param otherCoordinate
     * @return Euclidean distance
     */
    public double getDistanceEuclidean(Coordinate otherCoordinate){
        double distance = 0;
        distance = Math.sqrt((Math.pow((this.getLatitude()-otherCoordinate.getLatitude()),2)
                + Math.pow((this.getLongitude()-otherCoordinate.getLongitude()),2)));
        return distance * 111;
    }

    /**
     * Overrides the toString method and creates a String representation of this object.
     * @return string representation
     */
    public String toString(){
        return "(" + getLatitude() + ", " + getLongitude() + ")";
    }

}
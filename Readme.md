## Test header

- **Teacher** Michiel Noback (NOMI), to be reached at +31 50 595 4691
- **Test size** 4 assignments
- **Aiding materials** Computer on the BIN network
- **Supplementary materials**  
    - `docs/corejava.pdf` Java cheat sheet  
    - `docs/RefCardz_designpatterns.pdf` Design Patterns cheat sheet  
    - `docs/Java8_cheat_sheet.pdf` Java 8 cheat sheet (Lambda's and Streams)  

- **TO BE SUBMITTED  
This project, as `zip` archive. Use this name for the zip:
 `AppdesignAssessment<YOURLASTNAME-YOURSTUDENTNUMBER>.zip`**
 

## Instructions

#### To get started
- Start IntelliJ (start menu - development), accept trial version and default settings 
- Open the assessment as an Idea project - you will find it on your Desktop. **Do not open by selecting the build.gradle file, but simply select the parent folder!**
- You can ignore the VCS notes, or get rid of them by clicking "configure" and removing the VCS support

#### When you are finished
After finishing, zip your project folder as  `AppdesignAssessment<YOURNAME-YOURSTUDENTNo>.zip` and submit it using the submit script `submit_your_work`. Type `submit_your_work --help` in the terminal to get help on its usage.

The possible number of points to be scored are indicated for all assignments. Your grade 
will be calculated as  
`double grade = (total/maximumPoints * 10) < 1 ? 1 : (total/maximumPoints * 10);`


## The Assignments

### Assignment A: JUnit testing 25 points)
Package `nl.bioinf.appdesign.a_test` contains class `Coordinate` This is a
simple class with only three public methods: `getLatitude()`, `getLongitude()` and
`getDistanceEuclidean()`. Basic functionality has already been implemented.
The test class `CoordinateTest` is located in the test folder. One example test is already present.

It is your task to create test cases that cover all three methods to ensure full test coverage of both
sunny-day scenarios and boundary cases (think about the nature of the latitude and longitude parameters!).
All tests should pass when you are finished, so you may have to adjust the production code to
 make this happen.

### Assignment B: Refactoring (30 points)
In package `nl.bioinf.appdesign.b_refactor` you will find classes `Rectangle` that is quite smelly.
It is your job to refactor this class so that it adheres to the basic OO design
 principles (especially the SOLID principles).  
 
 This may (or may not) involve any refactoring technique:
 - renaming
 - extracting methods, (inner) classes, enums
 - implementing a design pattern

**NB** The tests found in `RectangleTest` are correct as they are - the functionality in itself is OK and needs not be changed.
You may, however, change or add tests if you think that is required by your refactoring.


### Assignment C: Design Patterns (25 points)
In package `nl.bioinf.appdesign.c_designpattern` you will find class `Fermenter`. This class models a microbial fermenter
in a laboratory setting. There are three pieces of peripheral equipment that are able to help the fermenter from reaching
fatal conditions: a Cooler, a GlucoseInjector and a PhAdjuster. Each of these devices needs to be updated of change in the
Fermentor state so that they can act before it is too late: the Fermentor needs to have the internal state between these boundaries:

- glucose between 30 and 100 millimolar
- pH between 4 and 8
- temperature between 33 and 40 degrees celcius

Of course, as in all Fermentors, Glucose and pH only goe down and temperature goes up.

**It is your job to implement the right Design Pattern that supports keeping the fermenter in steady state (in balance).**

You may add classes, interfaces, enums and change existing code, but you are not allowed to change
the functionality of `addBase()`, `cool()` and `addGlucose()`. Nor are you allowed to change the essence of
method `ferment()` - only add a few lines of code if required.

There is a single test method in package `test/java/nl.bioinf.appdesign.c_designpattern` that you can use to test and demonstrate
your functionality.

### Assignment D: Streams and Lambdas (20 points)

You should solve these assignments using streams and lambdas. However, for both assignments you are encouraged to create 
  relevant helper methods. 
  In package `nl.bioinf.appdesign.d_streams_lambdas` you will find class `StreamAssignment` that contains two method 
  stubs to be implemented.  

1. **(10 points)**  Using field `SNP_COLLECTION`, implement method `getDiseaseCandidateSnps`. It should should output 
      the SNPs with a minor allele frequency between 0.0001 and 0.1, as CSV formatted String.  
      Output should be something like this:  
         
        ```
         100275;A;C;0.00323
         162889;C;G;8.72E-4
         676255;G;C;0.0016672
         667280;A;G;0.00287
         719876;C;A;0.006649
        ```
2. **(10 points)** Using field `SNP_DATA`, implement method `getTransversionsTransitions`. It should return a Map. 
      Keys of this Map should be the Strings "transversion" and "transition", and the values of the Map should be 
      lists of SNP objects. "Transitions" are changes between A<->G and C<->T and "transversions" 
      are changes A<->C, G<->T, A<->T and C<->G.

**END OF TEST**